//Wemos LED Pins
#define LED_PIN_RED   14 //D5
#define LED_PIN_GREEN 12 //D6
#define LED_PIN_BLUE  13 //D7

int redValue = 0;
int greenValue = 0;
int blueValue = 0;

int animState = 0;

void setup() {
}

void loop() {
    redValue = ((animState + 2) % 2) * 255;
    greenValue = ((animState/2) % 2) * 255;
    blueValue = ((animState/4) % 2) * 255;
    animState++;

    analogWrite(LED_PIN_RED, redValue);
    analogWrite(LED_PIN_GREEN, greenValue);
    analogWrite(LED_PIN_BLUE, blueValue);

    delay(150);
}
