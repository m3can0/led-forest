#include <ESP8266WiFi.h>            
#include <ESP8266WebServer.h>

//Wemos LED Pins
#define LED_PIN_RED   14 //D5
#define LED_PIN_GREEN 12 //D6
#define LED_PIN_BLUE  13 //D7

#define NETWORK_SSID "LedForest"
#define NETWORK_PASSWORD ""

#define CONTENT_TYPE_JSON "application/json"

#define STATE_OFF "off"
#define STATE_ON "on"

IPAddress ip(192,168,7,1);
IPAddress gateway(192,168,7,1);
IPAddress subnet(255,255,255,0);

ESP8266WebServer server(80);

String state = STATE_OFF;
int redValue = 0;
int greenValue = 0;
int blueValue = 0;

void setup() {
    Serial.begin(115200);
    Serial.println();
    
    Serial.print("Setting access point");
    WiFi.mode(WIFI_AP);
    Serial.print("Access point config: ");
    Serial.println(WiFi.softAPConfig(ip, gateway, subnet) ? "Ready" : "Failed!");
    Serial.print("Access point state: ");
    Serial.println(WiFi.softAP(NETWORK_SSID, NETWORK_PASSWORD) ? "Ready" : "Failed!");

    // Connection established to WiFi
    Serial.print("Access point started with address: ");
    Serial.println(WiFi.softAPIP());

    // Identify route for GET request
    server.on("/led", handleRouteLed);

    // Start server on the microcontroller
    server.begin();                                       
    Serial.println("Server started ;)");  
}

void loop() {
    // Listen for client request ex:Webpage, phone, etc... 
    server.handleClient();
}

void handleRouteLed() {
    Serial.println("handleLedRoute()");   
    // Read mandatory argument State
    String argumentState = server.arg("state");

    //When only /led we return a success with the state of the rgb led
    if(argumentState == "") {
      sendSuccess();
      return;
    }

    // Check if there is a state argument and if it's different from on and off
    // If so return an message with an error code
    if (!(argumentState.equals(STATE_ON) || argumentState.equals(STATE_OFF))) {
      // Return an error message to the client if State on or off not found
      sendRequestError("Mandatory argument missing.\nRequest should be led?State=on or led?State=off");
      return;
    }
    state = argumentState;

    //Read optional arguments for the led color
    String argumentRed = server.arg("r");
    if(argumentRed != "") {
        redValue = argumentRed.toInt();
    }
    
    String argumentGreen = server.arg("g");
    if(argumentGreen != "") {
        greenValue = argumentGreen.toInt();
    }
    
    String argumentBlue = server.arg("b");
    if(argumentBlue != "") {
        blueValue = argumentBlue.toInt();
    }

    if(state.equals(STATE_ON)) {
        analogWrite(LED_PIN_RED, redValue);
        analogWrite(LED_PIN_GREEN, greenValue);
        analogWrite(LED_PIN_BLUE, blueValue);
    }
    else {
        analogWrite(LED_PIN_RED, 0);
        analogWrite(LED_PIN_GREEN, 0);
        analogWrite(LED_PIN_BLUE, 0);
    }
    
    sendSuccess();
}

void sendRequestError(String error) {
    // Create the Json error
    String errorJsonMessage = "{error=\"" + error + "\"}";
    Serial.println(errorJsonMessage); 
    server.send(400, CONTENT_TYPE_JSON, errorJsonMessage);
}

void sendSuccess() {
    // Create the Json response
    String successJsonMessage = "{state=\"" + state + "\"";
    successJsonMessage += ",r=" + (String)redValue;
    successJsonMessage += ",g=" + (String)greenValue;
    successJsonMessage += ",b=" + (String)blueValue;
    successJsonMessage += "}";
    Serial.println(successJsonMessage); 
    server.send(200, CONTENT_TYPE_JSON, successJsonMessage);
}
