# Leds Forest / Forêt Dels

* Level: Intermiate
* Cost: $45 CAD
* Time: 8h - 12h

The Led Forest Project is an introduction to Iot(Internet of thing) electronic controled by an Android App. 

This project will introduce you to setting up a simple REST server in a ESP based microcontroller. 
This server will be handling a GET request to send and receive information about the state of the Leds. 

You will be creating a client Android App that will show the state of the Leds and to send commands to turn on/off and change the color of the Leds.

In this tutorial is splitted in three part:
1. Circuits.

   In the electronic part, you will learn to create a circuit to drive 12V RGB Leds using a 3.3 or 5V output.
   
2. Arduino.

   In the Wemos programming section, you will learn how to implement the simple REST server in your microcontroller.
   
3. Android.

   Finally, you will be introduce to handle REST request in an Android App using the Volley library.
   
I suggest that you follow the suggested order, but the two first part are indenpendant. 
You'll find an Arduino Sketch in the electronic folder that can control RGB Led on any Arduino base microcontroller.  
In the Wemos folder you'll find a simple 3 Leds circuit that can be use as a benchmark to test the server.
Sadly, the Android part is dependent on the Wemos server to create a WiFi communication.

---

* Niveau: Intermédiaire
* Coût: 45$ CAD
* Temps: 8h - 12h

Le projet Forêt de Dels est une introduction au objet connecté dit iot(internet des objets) controllé par une App Android.

Ce projet vous introduira à la création d'un petit serveur REST à l'aide dèun microcontroleur de type ESP.
Ce serveur traite une requête de type GET pour envoyer et recevoir l'états des Dels RGB. 
Nous verons comment recevoir les états(on/off, couleurs RGB) des Dels. 
Nous modifierons aussi ces dernières en envoyant la commande au Wemos.

Pour Android, nous crérons une application client qui nous permettra d'afficher l'états des Dels à l'écran. 
Nous ajouterons aussi des boutons pours modifier les couleurs et allumer et éteindre les Dels.

Ce tutoriel ce divisera en trois sections:
1. Circuits.

   Dans la partie électronique, vous apprendrez à créer un circuit pour contrôler des DELs 12V à l'aide d'un signal 3.3 ou 5V.
   
2. Arduino.

   Dans la section de la programmation Wemos, vous apprendrez à implémenter un petit server REST à l'aide du micocontrôleur.
   
3. Android.

   Finalement, vous serez introduit au traitement de requête REST dans une application Adnroid à l'aide de la librairie Volley.

L'ordre ci-dessus est celui suggéré, mais les parties un et deux sont indépendantes.
Dans la dossier "électronique", vous retrouverez un Sketch Arduino qui ne demande aucun server pour fonctionner. 
Ce dernier peut être fonctionner avec n'importe quel microcontroleur de type Arduino.
De même, dans le dossier "Wemos", vous retrouverez les infomations pour faire un circuit de tests qui ne demande que quelques pièces et un Breadboard.
Malheureusement, la section "android" est dépendante du serveur de la section 2(Wemos) pour créer un canal de communication WiFi.

## Getting Started / Pour Débuter

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
First of all, clone or download the project on your computer.

---

Ces instruction vous permettrons de faire fonctionner ce projet sur votre machine locale à des fin de test et développement.
pour commencer, cloner ou télécharger ce projet sur votre ordinateur.

#### Git Link / Lien Git
```
https://gitlab.com/m3can0/led-forest.git
```

----

## Table of content / Sommaire

1. [Circuits prerequisites / Prérequis circuits](#circuits)

   1.1. [12V Leds control circuit / 12V Circuit de contrôle de Dels 12V](#circuit_12v_control)
   
   1.2. [Simple Led circuit / Simple circuit de Dels](#circuit_simple_led)

2. [Microcontroler prerequisites / Prérequis microcontrôleur](#arduino) 
 
   2.1. [Install Wemos library / Installer la librairie Wemos](#wemos_lib)

   2.2. [Network setup / Configuration du réseau](#network_setup)
   
   2.3. [Api informations / Informations sur l'API](#api_info)
   
   2.4. [Try the Sketch / Essayer le Sketch](#try_sketch)

3. [Android prerequisites / Prérequis Android](#android)  

   3.1 [Cellphone configuration / Configuration du téléphone](#android_phone_setup)  
   
   3.2 [Try the application / Essayer l'application](#android_try_app) 
----

<a name="circuits"/>

## Circuits prerequisites / Prérequis circuits

In the [circuits](https://gitlab.com/m3can0/led-forest/blob/master/circuits) folder you will find two differents circuit. 

---

Dans le dossier [circuits](https://gitlab.com/m3can0/led-forest/blob/master/circuits) vous trouverez deux circuits différents. 

<a name="circuit_12v_control"/>

#### 12V Leds control circuit / 12V Circuit de contrôle de Dels 12V

First you will find the circuit the control the 12V Led in [circuit_12v_led_controller.png](https://gitlab.com/m3can0/led-forest/blob/master/circuits/circuit_12v_led_controller.png).

###### Required components

* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Wifi prototyping platform
* Breadboard
* Wires
* 3 10K Ohms Resistors (brown, black, orange)
* 3 Mosfet Transistors 30N06LE
* 1 meter of 12V RGB Leds
* 12V 2A power supply

The control circuit use Mosfet transistors that act as a switch to power the 12V Leds. 
The trigger to turn the transistoris a 3.3 or 5V digital output. 

---

Dans un premier temps vous trouverez le circuit de contrôle de Dels 12V dans [circuit_12v_led_controller.png](https://gitlab.com/m3can0/led-forest/blob/master/circuits/circuit_12v_led_controller.png).

###### Composants necessaires

* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Plateforme de prototypage Wifi
* Breadboard
* Fils
* 3 Résistances de 10K Ohms (brun, noir, orange)
* 3 Transistor Mosfet 30N06LE
* 1 mètre de Dels 12V RGB
* Alimentation 12V 2A

Le circuit de contrôle utilise des transistors de type Mosfet qui agissent comme interrupteur pour alimenter les Dels de 12V.
Le déclencheur pour modifier l'état des transistors est une sortie digitale de 3.3 ou 5V.

<a name="circuit_simple_led"/>

#### Simple Led circuit / Simple circuit de Dels

If you want to test the arduino server without the 12V Leds RGB Circuit, you can use [circuit_simple_led.png](https://gitlab.com/m3can0/led-forest/blob/master/circuits/circuit_simple_led.png).

###### Required components

* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Wifi prototyping platform
* Breadboard
* Wires
* 3 220 Ohms Resistors (red, red, black)
* 3 Leds

This circuit use 3 leds to simulate the RGB led. It can be drive by a 3.3 or 5V digital output directly.

---

Si vous voulez utiliser le serveur Arduino sans le circuit de contrôle de dels 12V vous pouvez construire le circuit de Dels [circuit_simple_led.png](https://gitlab.com/m3can0/led-forest/blob/master/circuits/circuit_simple_led.png)

###### Composants necessaires

* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Plateforme de prototypage Wifi
* Breadboard
* Fils
* 3 Résistances de 220 Ohms (rouge, rouge, noir)
* 3 Dels

Ce circuit utilise 3 dels pour simuler une del RGB. Il peut être contrôlé par une sortie digitale 3.3 ou 5V.

<a name="arduino"/>

## Microcontroler prerequisites / Prérequis microcontrôleur

In the [arduino](https://gitlab.com/m3can0/led-forest/tree/dev/arduino) folder you will find tree Arduino Sketch:

* [led_simple_animation](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_simple_animation) A simple animation to test the RGB circuit.
* [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) A server that use a local network to control LEDs.
* [led_forest_access_point](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_access_point) A server acting as an access point or router to control the LEDs. 

###### Electronic hardware

* Wifi Router 
* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Wifi prototyping platform

You will need a Wemos board. The sketch can run on others ESP board, 
but you might need to install additionnal driver or library depending on the used microcontroler.

###### Software

For the microcontroler section you will need the Arduino IDE Software to edit the Arduino sketch and upload it to the Wemos.

* [Arduino IDE](https://www.arduino.cc/en/Main/Software) - Integrated Development Environment

---

Dans le dossier [arduino](https://gitlab.com/m3can0/led-forest/tree/dev/arduino) vous trouverez trois Sketch Arduino:

* [led_simple_animation](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_simple_animation) Une animation pour tester le circuit RGB.
* [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) Un serveur qui utilise un réseau local pour contrôler les DELs.
* [led_forest_access_point](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_access_point) Un serveur qui agit comme point d'accès ou routeur pour contrôler les DELs.


###### Matériel électronique

* Routeur Wifi
* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Plateforme de prototypage Wifi

Vous aurex besoin de la plateforme de prototypage Wemos D1 mini lite. Le sketch fonctionne sur d'autres micocontrôleur de type ESP, 
mais vous aurez peut-être besoin de librairies ou de pilotes particuliers dépendament de votre microcontrôleur.

###### Logiciel 

Pour la section sur le micocontrôleur, vous aurez besoin de l'environment de développement Arduino. 
Ce dernier vous permettra de modifier le Sketch Arduino ainsi que le téléverser sur votre Wemos. 

* [ IDE Arduino](https://www.arduino.cc/en/Main/Software) - Environement de développement

<a name="wemos_lib"/>

#### Install Wemos library / Installer la librairie Wemos

To use Esp8266 board, you will need to add a specific library to the Arduino IDE.

1. Open the Arduino IDE.

2. We will have to add path to Download the Esp8266 library.
In the `Arduino Preferences`, you will find field name `Additional Boards manager URLs`. 
Add the following url to have access to the Esp8266 library.
  
   ```
   http://arduino.esp8266.com/stable/package_esp8266com_index.json
   ```

3. Open the board manager window by clicking `Boards Manager...` under `Tools->Board`.

4. In the `Filter your search` type `esp`. You should find a library called `esp8266 by ESP8266 Community`. Select the latest version and press the `Install` button.

5. After the library is completely installed, close and restart the IDE.

6. If the installation was successfull, you should have see the `ESP 8266 Modules` under `Tools->Board`.

---

1. Ouvrir l'IDE d'Arduino.

2. Ajouter l'url pour trouver les informations sur la librairie Esp8266.
Dans `Arduino Preferences`, vous retrouvez le champ `Additional Boards manager URLs`. 
Ajouter l'url suivant dans le champ pour avoir accès à la librairie Esp8266.
  
   ```
   http://arduino.esp8266.com/stable/package_esp8266com_index.json
   ```

3. Ouvrir la fenêtre de gestion des plateformes en selectionnant `Boards Manager...` sous `Tools->Board`.

4. Dans le champ `Filter your search` écrivez `esp`. Vous devriez trouver un librairie nommée `esp8266 by ESP8266 Community`. Sélectionner la dernière version et appuyer le bouton `Install`.

5. Après que la librairie soit installée fermer et redémarrer l'IDE d'Arduino.

6. Si l'installation est un succès, vous devriez voir une section nommée `ESP 8266 Modules` sous `Tools->Board`.

<a name="network_setup"/>

#### Network setup / Configuration du réseau

To use the [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) Sketch, you will need to setup the network configuration of the Sketch.

* Change the variable `NETWORK_SSID` value by replacing `YourNetworkName` with your network name.

* Change the variable `NETWORK_PASSWORD` value by replacing `YourNetworkPassword` with your network password.

---

Pour utiliser le Sketch [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) vous devrez modifier les configurations du réseau à l'intérieur de ce dernier.

* Modifier la valeur de la variable `NETWORK_SSID` en remplaçant `YourNetworkName` avec le nom de votre réseau.

* Modifier la valeur de la variable `NETWORK_PASSWORD` en remplaçant `YourNetworkPassword` avec le mot de pase de votre réseau.

<a name="api_info"/>

#### Api informations / Informations sur l'API

   The folder [postman(folder)](https://gitlab.com/m3can0/led-forest/tree/master/postman) have a collection of all the server request that can be imported in the [Postman(software)](https://www.getpostman.com/).
   
   ---
   
   Le dossier [postman(dossier)](https://gitlab.com/m3can0/led-forest/tree/master/postman) contient toutes les requêtes du server. 
   Cette dernière s'implorte dans le logiciel [Postman(logiciel)](https://www.getpostman.com/).

**URL / URL:** /led

**Method / Méthode:** `GET`
  
**URL Params / Paramètres URL:**

    /led?state=on&r=255&g=255&b=255

   **Required / Requis:**
 
   No Mandatory param. / Aucun paramètre obligatoire.

   **Optional / Optionnel:**
   
   `state=[string]` 
   
   The parameter `state` will turn on and of the LEDs. it can only take the values of `on` or `off`. 
   Without this param, the Wemos will return the current state of the LEDs.
   
   * `on` will turn the lignt on. 
   * `off` will turn them off.
   
   ---
   
   Le paramètre `state`gère l'état des DELs. Elle ne prend que `on` et `off` comme valeurs.
   Sans ce paramètre, le Wemos retournera l'état des DELs à l'instant présent.

   * `on` allume les DELs. 
   * `off` ferme les DELs.
   
   ---
 
   `r=[interger]` | `g=[interger]` | `b=[interger]`
   
   The parameters `r`, `g` and `b` change the colors of the LEDs. The values must be between 0 and 255.
   
   * `r` will change the red value. 
   * `g` will change the green value.
   * `b` will change the blue value.
   
   ---
   
   Les paramètres `r`, `g` et `b` gèrent la couleur des DELs. Les valeurs doivent être inclusent entre 0 et 255.

   * `r` modifie la valeur de la couleur rouge. 
   * `g` modifie la valeur de la couleur verte. 
   * `b` modifie la valeur de la couleur bleu. 
  
**Body Params / Paramètres de la requête**

  No Body in the request. / Aucune données dans le corps de la requête.

**Success Response / Réponse positive:**
  
  **Code / Code:** 200 <br />
  **Content / Contenue:**
  ```
  {state="on",r=100,g=200,b=50}
  ```
  
  When the request is successfull, the Wemos return the current state and colors of the LEDs.
  
  ---
  
  Losque la requête réussit, le Wemos retourne les valeurs des paramètres des DELs.
 
**Error Response / Réponse erreur:**

  **Code / Code:** 400 Bad Request error <br />
  **Content / Contenue:**
  ```
  {error="<ERROR_MESSAGE>"}
  ```
  
  The only error case handled is when the state has a different value then `on` or `off`
  
  ---
  
  Le serveur retourne une erreur si le paramètre `state` à une valeur différente de `on` ou `off`

<a name="try_sketch"/>

#### Try the Sketch / Essayer le Sketch 

It's now time to have some fun and light up some LEDs ;).

1. Upload the Sketch to your Wemos.
   
   Make sure you selected the right Board `(LOLIN WEMOS) D1 mini Lite` and the right usb port for your wemos.

2. After upload completion, open the `Serial monitor` (magnify glass icon). 

3. Note the `IP Address` of your Wemos server.

4. Connect your computer to the server.

   A. For the server [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) using your local Wifi router. 
      Make sure your computer and the Wemos are connected on the same router.
      
   B. For the access point [led_forest_access_point](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_access_point), 
      you will need to connect your computer to the WiFi named `LedForest`.
     
5. Open your web browser or [Postman](https://www.getpostman.com/).  
6. Replace `<YOUR_ULR>` with the Wemos `IP Address` in the following Url.

   ```
   http://<YOUR_ULR>/led?state=on&r=100&g=200&b=50
   ```

7. Copy paste the Url in your web browser or [Postman](https://www.getpostman.com/). This swill turn the LEDs on.
   * To turn off the LEDs, change `on` state for `off`
   * To change the colors of the LEDs, change the value next to `r`, `g` or `b` between 0 and 255.

---

Il est maintenant temps de s'amuser un peu avec les DELs ;)

1. Téléverser le Sketch sur votre Wemos.
   
   Assurez-vous d'avoir sélectionner la bonne plateforme `(LOLIN WEMOS) D1 mini Lite` et le bon port usb de votre Wemos

2. Après la réussite du téléversement, ouvrir le `Serial monitor` (icône loupe).

3. Noter  `Addresse IP` de votre serveur Wemos.

4. Connecter votre ordinateur au serveur.

   A. Pour le serveur [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) qui utilise votre réseau local,
      Assurez vous que l'ordinateur et le Wemos soit sur le même réseau.
      
   B. Pour le poit d'accès [led_forest_access_point](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_access_point), 
      connecter votre ordinateur sur le réseau nommé `LedForest`.
     
5. Ouvrir votre navigateur web ou [Postman](https://www.getpostman.com/).  
6. Replacer `<VOTRE_URL>` avec `Addresse IP` de votre Wemos and Url suivant:

   ```
   http://<VOTRE_URL>/led?state=on&r=100&g=200&b=50
   ```

7. Copier-coller l'url dans votre navigateur web ou dans [Postman](https://www.getpostman.com/). Cette dernière allume les DELs.
   * Pour éteindre les DELs, modifier la valeur à droite de la variable `state` de `on` à `off`.
   * Pour modifier les coules des DELs, modifier une des valeurs à droite de `r`, `g` ou `b` entre 0 et 255.

<a name="android"/>

## Android prerequisites / Prérequis Android

In the [android_lf](https://gitlab.com/m3can0/led-forest/tree/master/android_lf) folder you will find the complete Led Forest application to run on your Android phone.

###### Electronic hardware

* Android phone

###### Software

   To modify and test the Android app you will need the Android IDE.

   * [Android Studio](https://developer.android.com/studio) - Android Integrate Development Environment(IDE)

---

Dans le dossier [android_lf](https://gitlab.com/m3can0/led-forest/tree/master/android_lf) vous retrouverez l'application Android que vous pourrez executer sur votre téléphone Android.

###### Matériel électronique

* Téléphone Android

###### Logiciel 

   Pour modifier l'application Android pour vos besoins vous devrez installer l'IDE Android Studio.

   * [Android Studio](https://developer.android.com/studio) - Environement de développement intégré Android(IDE)

<a name="android_phone_setup"/>

#### Cellphone configuration / Configuration du téléphone

To load the application on Android phone, the `USB debugging` option must be enabled from the `Developer options`.</br>
Follow Android documentation to have access to complete the configuration of the phone.

* [Configure on-device developer options](https://developer.android.com/studio/debug/dev-options)

---

Pour téléverser l'application sur votre téléphone, l'option `USB debugging` doit être activée du menu `Developer options`.</br>
Utiliser la documentation Android pour compléter la configuration de votre téléphone.

* [Configure on-device developer options](https://developer.android.com/studio/debug/dev-options)

<a name="android_try_app"/>

#### Try the application / Essayer l'application

It's finally time to test the whole project.

1. Connect your phone to the right network.

   A. For the server [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) using your local Wifi router. 
      Make sure your computer and the Wemos are connected on the same router.
      
   B. For the access point [led_forest_access_point](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_access_point), 
      you will need to connect your phone to the WiFi named `LedForest`.

2. Upload the Application on the Android phone or Emulator.</br> 
   If it's your first time to run an app, follow Android tutorial: [Run apps on a hardware device](https://developer.android.com/studio/run/device) or [Run apps on the Android Emulator](https://developer.android.com/studio/run/emulator).
   
3. After the application has been install and launch on the phone or emulator, a dialog will ask for the `Server Address`.</br> 
   Enter the `IP Address` of the Wemos server from the [Try the Sketch / Essayer le Sketch](#try_sketch).

4. Press the three to turn on and off the LEDs and use the sliders to change to colors.
 
---

Il est maintenant temps de tester le projet complêt.

1. Connecter votre téléphone au bon réseau.

   A. Pour le serveur [led_forest_server](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_server) qui utilise votre réseau local,
      Assurez vous que l'ordinateur et le Wemos soit sur le même réseau.
      
   B. Pour le poit d'accès [led_forest_access_point](https://gitlab.com/m3can0/led-forest/tree/dev/arduino/led_forest_access_point), 
      connecter votre ordinateur sur le réseau nommé `LedForest`.

2. téléverse votre application sur votre téléphone Android ou sur l'émulateur.</br> 
   Si c'est vous téléverser une application pour la première fois, référez vous au tutoriels d'Android: [Run apps on a hardware device](https://developer.android.com/studio/run/device) ou [Run apps on the Android Emulator](https://developer.android.com/studio/run/emulator).
   
3. Après l'installation et le démarrage de l'application sur votre téléphone ou l'émulateur, une boite de dialogue vous demande `L'adresse du serveur`.</br>  
   Enter y l'`Adresse IP` de votre serveur Wemos notée à dans la [Try the Sketch / Essayer le Sketch](#try_sketch).

4. Appuyer sur l'arbre pour allumer et éteindre les DELs. Utiliser les curseurs pour modifier les couleurs.

## References / Références
* [Arduino](https://www.arduino.cc) - Website to learn more about the Arduino platform and language informations
* [Wemos D1 mini lite](https://wiki.wemos.cc/products:d1:d1_mini_lite) - Wifi prototyping platform based on ESP-8285 microcontroller.

---

* [Postman](https://www.getpostman.com/) - Api development/test software

---

* [Android Studio](https://developer.android.com/studio/index.html) - Android IDE
* [Android Developer Guides](https://developer.android.com/guide) - Android development documentation
* [Android Volley Library](https://developer.android.com/training/volley) - Android library to handle REST resquest.

## Version / Version

See [Tags](https://gitlab.com/m3can0/led-forest/tags) to know the latest change in the versions.

---

Voir [Tags](https://gitlab.com/m3can0/led-forest/tags) pour avoir les informations sur les derniers changements dans les versions.

## Authors / Autheurs

* **Nataniel P-Montpetit** - *Initial work*

See also the list of [contributors](https://gitlab.com/m3can0/led-forest/graphs/master) who participated in this project.

---

Aussi voir la liste de [contributeurs](https://gitlab.com/m3can0/led-forest/graphs/master) qui ont participés au projet.

## License / License

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
## Acknowledgments / Remerciements 

Thanks to all the people who made this possible.

---

Merci à toutes les personnes exceptionnelles qui ont rendu cela possible.

* Stéphane Hoareau - [Timecode Lab](http://www.timecodelab.com)
* Miriam Danielsson - Designer
* Audrey Gagnon - Designer

Copyright © 2019 Nataniel Poupart-Montpetit
