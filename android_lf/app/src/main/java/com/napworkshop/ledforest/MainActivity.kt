package com.napworkshop.ledforest

import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.napworkshop.ledforest.databinding.ActivityMainBinding

/**
 * Reference for Volley
 * https://developer.android.com/training/volley
 *
 * Add dependency
 */

class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding
        get() = _binding!!

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(this, MainViewModel.Factory(applicationContext))[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater).apply {
            viewModel = mainViewModel
            lifecycleOwner = this@MainActivity
        }
        setContentView(binding.root)

        with(binding) {
            redSeekBar.setOnSeekBarChangeListener(this@MainActivity)
            greenSeekBar.setOnSeekBarChangeListener(this@MainActivity)
            blueSeekBar.setOnSeekBarChangeListener(this@MainActivity)
        }

        binding.ledImageButton.setOnClickListener {
            mainViewModel.switchLedOnOffState()
        }

        mainViewModel.ledBackgroundColor.observe(this) { ledColor ->
            when (val background = binding.ledImageButton.background) {
                is ShapeDrawable -> background.paint.color = ledColor
                is GradientDrawable -> background.setColor(ledColor)
                is ColorDrawable -> background.color = ledColor
            }
        }

        mainViewModel.serverMessageState.observe(this) { state ->
            binding.serverState.setCompoundDrawablesWithIntrinsicBounds(state.iconRes, 0, 0, 0)
            binding.serverState.text = getString(state.messageRes, *state.messageArgs)
            binding.serverState.setBackgroundColor(getColor(state.backgroundColorRes))
        }

        binding.serverState.setOnClickListener {
            ServerAddressDialogFragment.show(supportFragmentManager)
        }

        mainViewModel.error.observe(this) { exception ->
            showErrorMessage(exception.message ?: "")
        }
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.updateIpAddressStateMessage()
        mainViewModel.getLatestLedState()
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        when (seekBar.id) {
            R.id.redSeekBar -> mainViewModel.setRedState(progress)
            R.id.greenSeekBar -> mainViewModel.setGreenState(progress)
            R.id.blueSeekBar -> mainViewModel.setBlueState(progress)
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) { }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        mainViewModel.setLedState()
    }

    private fun showErrorMessage(exceptionDetails: String) {
        val errorMessage = getString(R.string.toast_server_error, exceptionDetails)
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
