package com.napworkshop.ledforest.data

import android.content.Context
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.napworkshop.ledforest.data.ILedApi.Companion.ERROR_MISSING_SERVER_ADDRESS
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * LedApi
 * Classe qui fait quelque chose
 *
 * @author m3can0
 * @created 2019-05-12
 */

interface ILedApi {
    companion object {
        const val STATE_ON = "on"
        const val STATE_OFF = "off"

        const val ERROR_MISSING_SERVER_ADDRESS = "LedApi missing server address"
    }

    @Throws(Exception::class)
    suspend fun getLedState(serverDomain: String): Led

    @Throws(Exception::class)
    suspend fun setLedState(serverDomain: String, led: Led): Led

    fun cancelLedRequest()
}

class LedApi(appContext: Context) : ILedApi {

    companion object {
        const val TAG_LED_REQUEST = "ledRequest"
    }

    // Instantiate the RequestQueue.
    private val queue = Volley.newRequestQueue(appContext)
    private val gson : Gson by lazy { Gson() }

    @Throws(Exception::class)
    override suspend fun getLedState(serverDomain: String): Led {
        return getRequest(serverDomain, "/led")
    }

    @Throws(Exception::class)
    override suspend fun setLedState(serverDomain: String, led: Led): Led {
        var contextUrl = "/led?state=${led.state}"
        contextUrl += "&r=${led.redValue}"
        contextUrl += "&g=${led.greenValue}"
        contextUrl += "&b=${led.blueValue}"

        return getRequest(serverDomain, contextUrl)
    }

    private suspend fun getRequest(serverDomain: String, contextUrl: String): Led {
        if (serverDomain.isEmpty()) {
            throw Exception(ERROR_MISSING_SERVER_ADDRESS)
        }
        // Create the request to send to the controller
        val ledUrl = "http://$serverDomain$contextUrl"

        return suspendCoroutine { continuation ->
            val stringRequest = StringRequest(
                Request.Method.GET, ledUrl,
                { response ->
                    try {
                        val led = gson.fromJson(response, Led::class.java)
                        continuation.resume(led)
                    }
                    catch (exception: Exception) {
                        continuation.resumeWithException(exception)
                    }
                },
                { error -> continuation.resumeWithException(error) }
            )
            stringRequest.tag = TAG_LED_REQUEST

            // Add the request to the RequestQueue.
            queue.add(stringRequest)
        }
    }

    override fun cancelLedRequest() {
        queue.cancelAll(TAG_LED_REQUEST)
    }
}