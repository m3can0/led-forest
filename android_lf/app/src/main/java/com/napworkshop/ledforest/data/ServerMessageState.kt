package com.napworkshop.ledforest.data

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.napworkshop.ledforest.R

sealed class ServerMessageState(
    @DrawableRes val iconRes: Int,
    @ColorRes val backgroundColorRes: Int,
    @StringRes val messageRes: Int,
    vararg val messageArgs: Any
) {
    class Setup : ServerMessageState(
        android.R.drawable.ic_dialog_alert,
        android.R.color.holo_orange_light,
        R.string.tv_server_state_setup_address
    )

    class Manage(ipAddress: String) : ServerMessageState(
        android.R.drawable.ic_menu_edit,
        android.R.color.holo_green_light,
        R.string.tv_server_state_manage_address,
        ipAddress
    )
}