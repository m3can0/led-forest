package com.napworkshop.ledforest.data

import android.content.Context
import android.content.SharedPreferences

interface IServerLocalData {
    var serverAddress : String?
}

class ServerLocalData(appContext: Context) : IServerLocalData {

    companion object {
        private const val PREF_LED_FOREST = "LedForestPref"
        private const val SERVER_ADDRESS = "ServerAddress"
    }

    private val sharedPref: SharedPreferences = appContext.getSharedPreferences(PREF_LED_FOREST, Context.MODE_PRIVATE)

    override var serverAddress : String?
        get() = sharedPref.getString(SERVER_ADDRESS, null)
        set(value) {
            with(sharedPref.edit()) {
                putString(SERVER_ADDRESS, value)
                apply()
            }
        }
}