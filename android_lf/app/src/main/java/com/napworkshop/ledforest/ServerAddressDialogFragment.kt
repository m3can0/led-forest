package com.napworkshop.ledforest

import android.os.Bundle
import android.text.Editable
import android.view.WindowManager
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.napworkshop.ledforest.databinding.FragmentServerAddressBinding


/**
 * ServerAddressDialogFragment
 * Classe qui fait quelque chose
 *
 * @author m3can0
 * @created 2019-05-13
 */
class ServerAddressDialogFragment : DialogFragment(), View.OnClickListener {

    companion object {
        private const val TAG = "ServerAddressDialogFragment"

        fun show(supportFragmentManager: FragmentManager) {
            // Check for a single
            if(supportFragmentManager.findFragmentByTag(TAG) != null) {
                return
            }
            val dialog = ServerAddressDialogFragment()
            dialog.show(supportFragmentManager, TAG)
        }
    }

    private var _binding: FragmentServerAddressBinding? = null
    private val binding: FragmentServerAddressBinding
        get() = _binding!!

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(requireActivity(), MainViewModel.Factory(requireActivity().applicationContext))[MainViewModel::class.java]
    }

    private var serverAddress : Editable
        get() = binding.serverAddressEditText.text
        set(value) { binding.serverAddressEditText.text = value }

    private fun setServerAddress(value: String) {
        binding.serverAddressEditText.setText(value)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.setTitle(R.string.title_server_address)
        _binding = FragmentServerAddressBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.updateButton.setOnClickListener(this)
        binding.cancelButton.setOnClickListener(this)

        setServerAddress(mainViewModel.serverAddress ?: "")

        binding.serverAddressEditText.requestFocus()
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.updateButton -> {
                val serverAddress = serverAddress.toString()
                mainViewModel.serverAddress = serverAddress
                mainViewModel.getLatestLedState()
                dismiss()
            }
            R.id.cancelButton -> {
                dismiss()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}