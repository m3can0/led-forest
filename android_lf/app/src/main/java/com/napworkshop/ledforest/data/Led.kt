package com.napworkshop.ledforest.data

import com.google.gson.annotations.SerializedName

/**
 * Led
 * Classe qui fait quelque chose
 *
 * @author m3can0
 * @created 2019-05-12
 */
data class Led(
    @SerializedName("state")
    var state: String,
    @SerializedName("r")
    var redValue: Int,
    @SerializedName("g")
    var greenValue: Int,
    @SerializedName("b")
    var blueValue: Int
)