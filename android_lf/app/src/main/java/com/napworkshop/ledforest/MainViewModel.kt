package com.napworkshop.ledforest

import android.content.Context
import android.graphics.Color
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.napworkshop.ledforest.data.*
import com.napworkshop.ledforest.data.ILedApi.Companion.STATE_OFF
import com.napworkshop.ledforest.data.ILedApi.Companion.STATE_ON
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

class MainViewModel(
    private val ledApi: ILedApi,
    private val serverLocalData: IServerLocalData,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    // Message
    val serverMessageState = MutableLiveData<ServerMessageState>()
    val isControlEnabled = MutableLiveData<Boolean>()

    // Control
    val ledState = MutableLiveData<String>() //Should be on or off
    val ledBackgroundColor = MutableLiveData<Int>()
    val ledRedState = MutableLiveData<Int>() //Should be between 0 ans 254
    val ledGreenState = MutableLiveData<Int>() //Should be between 0 ans 254
    val ledBlueState = MutableLiveData<Int>() //Should be between 0 ans 254

    val error = MutableLiveData<Exception>()

    var serverAddress: String?
        get() = serverLocalData.serverAddress
        set(value) {
            serverLocalData.serverAddress = value
            updateIpAddressStateMessage()
        }

    fun updateIpAddressStateMessage() {
        val savedServerAddress = serverAddress
        //TODO we could validate the IP address format
        if(validateIpAddress(savedServerAddress)) {
            serverMessageState.value = ServerMessageState.Manage(savedServerAddress!!)
            isControlEnabled.value = true
        }
        else {
            serverMessageState.value = ServerMessageState.Setup()
            isControlEnabled.value = false
        }
    }

    private fun validateIpAddress(ipAddress:String?) : Boolean {
        // This will validate format in the form of 192.168.10.1
        val zeroTo255Regex = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])"
        val ipAddressRegex = (zeroTo255Regex + "\\." + zeroTo255Regex + "\\."
                + zeroTo255Regex + "\\." + zeroTo255Regex)
        val ipAddressPattern = Pattern.compile(ipAddressRegex)
        return !ipAddress.isNullOrEmpty() && ipAddressPattern.matcher(ipAddress).matches()
    }

    fun getLatestLedState() {
        viewModelScope.launch {
            try {
                val savedServerAddress = serverAddress ?: return@launch
                Log.i(MainViewModel::class.java.simpleName, "Saved Server Address=> $savedServerAddress")
                val ledResponse = withContext(backgroundDispatcher) { ledApi.getLedState(savedServerAddress) }
                handleLedResponse(ledResponse)
            } catch (exception: Exception) {
                Log.e(MainViewModel::class.java.simpleName, exception.toString())
                error.value = exception
            }
        }
    }

    fun switchLedOnOffState() {
        val currentLedState = ledState.value
        ledState.value = if (currentLedState == STATE_ON) {
            STATE_OFF
        } else {
            STATE_ON
        }

        setLedState()
    }

    fun setRedState(progress: Int) {
        ledRedState.value = progress
        val greenValue = ledGreenState.value ?: 0
        val blueValue = ledBlueState.value ?: 0
        setLedBackgroundColor(progress, greenValue, blueValue)
    }

    fun setGreenState(progress: Int) {
        ledGreenState.value = progress
        val redValue = ledRedState.value ?: 0
        val blueValue = ledBlueState.value ?: 0
        setLedBackgroundColor(redValue, progress, blueValue)
    }

    fun setBlueState(progress: Int) {
        ledBlueState.value = progress
        val redValue = ledRedState.value ?: 0
        val greenValue = ledGreenState.value ?: 0
        setLedBackgroundColor(redValue, greenValue, progress)
    }

    fun setLedState() {
        val currentLedState = ledState.value ?: STATE_OFF
        val redValue = ledRedState.value ?: 0
        val greenValue = ledGreenState.value ?: 0
        val blueValue = ledBlueState.value ?: 0
        val led = Led(
            state = currentLedState,
            redValue = redValue,
            greenValue = greenValue,
            blueValue = blueValue
        )

        viewModelScope.launch {
            try {
                val savedServerAddress = serverAddress ?: return@launch
                Log.i(MainViewModel::class.java.simpleName, "Saved Server Address=> $savedServerAddress")
                val ledResponse = withContext(backgroundDispatcher) { ledApi.setLedState(savedServerAddress, led) }
                handleLedResponse(ledResponse)
            } catch (exception: Exception) {
                Log.e(MainViewModel::class.java.simpleName, exception.toString())
                error.value = exception
            }
        }
    }

    private fun handleLedResponse(ledResponse: Led) {
        ledState.value = ledResponse.state
        setLedBackgroundColor(ledResponse.redValue, ledResponse.greenValue, ledResponse.blueValue)

        ledRedState.value = ledResponse.redValue
        ledGreenState.value = ledResponse.greenValue
        ledBlueState.value = ledResponse.blueValue
    }

    private fun setLedBackgroundColor(redValue: Int, greenValue: Int, blueValue: Int) {
        val ledColor : Int = Color.rgb(redValue, greenValue, blueValue)
        ledBackgroundColor.value = ledColor
    }

    override fun onCleared() {
        super.onCleared()
        ledApi.cancelLedRequest()
    }

    class Factory(appContext: Context) : ViewModelProvider.Factory {
        private val ledApi by lazy { LedApi(appContext) }
        private val serverLocal by lazy { ServerLocalData(appContext) }

        override fun <T : ViewModel> create(modelClass: Class<T>): T = MainViewModel(ledApi, serverLocal) as T
    }
}